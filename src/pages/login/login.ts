import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { UserProvider } from '../../providers/user/user';
import { HomePage } from '../../pages/home/home';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  email: string = '';
  password: string = '';
  errors: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public API: ApiProvider,
    public User: UserProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login(event): void {
    this.API.login(this.email, this.password)
      .then((result) => {
        if (result.errors) {
          this.errors = result.errors[0];
        } else {
          this.User.setUser(result);
          this.navCtrl.setRoot(HomePage);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
}
