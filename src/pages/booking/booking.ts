import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import  moment  from 'moment';

declare var jQuery: any;

@Component({
  selector: 'page-booking',
  templateUrl: 'booking.html',
})
export class BookingPage {

  public options: Array<number>;
  public siestaDays: Array<string>;
  public booking_type: string = 'siesta';
  public counter: number = 0;

  public bookingDays: any = {
    in_time: {
      value: moment().format('MM/DD/YYYY'),
      active: true
    },
    out_time: {
      value: moment().add(1, 'd').format('MM/DD/YYYY'),
      active: false
    },
    active(el) {
      if (el == 'in_time') {
        this.in_time.active = true;
        this.out_time.active = false;
      } else {
        this.in_time.active = false;
        this.out_time.active = true;
      }
    }
  };


  eventSource: Array<any> = [
    {
      title: '',
      startTime: new Date(),
      endTime: new Date(moment().add(1, 'd').format()),
      allDat: true
    }
  ];
  viewTitle;
  isToday: boolean;
  calendar = {
    mode: 'month',
    currentDate: new Date()
  }; // these are the variable used by the calendar.

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.options = this.getOptions();
    this.siestaDays = this.getSiestaDays();
  }

  ionViewDidLoad() {
    jQuery('select').drum({
      onChange : (elem) => {
        if (jQuery(elem).attr('id') === 'in_time') {
          jQuery('#out_time').drum('setIndex', parseInt(jQuery(elem).val()) + 1);
        } else if (jQuery(elem).attr('id') === 'out_time') {
          let diff = parseInt(jQuery(elem).val()) - parseInt(jQuery('#in_time').val());
          console.log(diff);
          if (diff >= 0 && diff <=1) {
            jQuery(elem).drum('setIndex', parseInt(jQuery('#in_time').val()) + 1);
          }
        }

      }
    });
    jQuery('#in_time').drum('setIndex', moment().hour());
    jQuery('#out_time').drum('setIndex', moment().hour() + 2);
  }

  bookingType(e) {
    if (e.target.value == 'days') {
    } else {
      jQuery('select').drum();
    }
  }

  // Calendar start

  onViewTitleChanged(title) {
    this.viewTitle = title;
  }


  onCurrentDateChanged(event:Date) {
    // debugger;
    if (this.counter > 0 ) {
      if (this.bookingDays.in_time.active) {
        console.log(1);
        this.bookingDays.in_time.value = moment(event).format('MM/DD/YYYY');
        this.bookingDays.out_time.value = '';
        this.eventSource = [];
      } else {
        // debugger;
        console.log(2);
        this.bookingDays.out_time.value = moment(event).format('MM/DD/YYYY');
        this.eventSource = [{
          title: '',
          startTime: new Date(this.bookingDays.in_time.value),
          endTime: new Date(this.bookingDays.out_time.value),
          allDat: true
        }]
      }
    }
    console.log(this.counter);
    this.counter += 1;
    // console.log(event);
    // var today = new Date();
    // today.setHours(0, 0, 0, 0);
    // event.setHours(0, 0, 0, 0);
    // this.isToday = today.getTime() === event.getTime();
  }


  markDisabled = (date:Date) => {
    var current = new Date();
    current.setHours(0, 0, 0);
    return date < current;
  };
  // Calendar end

  test() {
    this.bookingDays.in_time.value = '';
  }


  public getOptionValue(option) {
    let period = (option <= 12) ? "AM" : "PM";
    if (option > 12) {
      option = option - 12;
    }
    return option + ":00 " + period;
  }

  private getOptions() {
    let options = [];
    for (let i = 1; i <= 24; i++ ) {
      options.push(i);
    }
    return options;
  }

  private getSiestaDays() {
    let d = moment();
    let days = [];
    for (let i = 0; i < 20; i++) {
      days.push(d.format('DD MMMM'));
      d = d.add(1, 'd');
    }
    return days;
  }

}
