import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { UserProvider } from '../user/user';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ApiProvider {

  //host: string = 'http://hober.roadlogica.com';
  host: string = 'http://localhost:3000';
  form: any;

  constructor(public user: UserProvider) {
    console.log('Hello ApiProvider Provider');
    this.form = new FormData();
  }

  login(email, password): Promise<any> {
    let params = this.prepareData({email: email, password: password});
    return fetch(this.host + `/api/v1/auth/sign_in`, {
        body: params,
        method: 'POST'
      }
    ).then((res) => {
      if (res.ok) {
        this.user.headers = this.getHeaders(res);
      }
      return res.json();
    })
  }

  private prepareData(data: any) {
    for (let key in data) {
      this.form.append(key, data[key]);
    }
    return this.form;
  }

  private getHeaders(response): any {
    return {
      'access-token': response.headers.get('access-token'),
      'client': response.headers.get('client'),
      'expiry': response.headers.get('expiry'),
      'token-type': response.headers.get('token-type'),
      'uid': response.headers.get('uid'),
    };
  }
}
