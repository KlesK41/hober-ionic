import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

@Injectable()
export class UserProvider {

  user: any;
  headers: any;


  constructor(public storage: Storage) {
    this.getFromStorage();
  }

  getUser() {
    return this.user;
  }

  setUser(user) {
    this.storage.set('user', user);
    this.storage.set('headers', this.headers);
  }

  private getFromStorage() {
    this.user = this.user || this.storage.get('user');
  }



}
